<?php

namespace Rapture\Generator;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Process\Process;

class CreatePackage extends GeneratorCommand
{
    protected $signature = 'make:package';
    protected $description = 'Generate a new package';
    protected $folder;
    protected $namespace;
    protected $package;
    protected $object;
    protected $icon;

    public function handle()
    {
        $this->folder = $this->ask('Which folder are you working out of?', 'packages');
        $this->namespace = $this->ask('Which namespace would you like?', 'Rapture');
        $this->package = $this->ask('What is your package called?', 'Skeleton');
        $this->object = $this->ask('What database model would you like created (singular)?', 'Bone');
        $this->icon = $this->ask('Which font awesome icon?', 'skull');

        if ($this->isReservedName($this->package)) {
            $this->error('The name "' . $this->package . '" is reserved by PHP.');

            return false;
        }

        // Clone git repo
        $process = new Process(['git', 'clone', 'git@bitbucket.org:raptureapp/skeleton.git', $this->getPath()]);
        $process->setTimeout(null);
        $process->setWorkingDirectory($this->laravel->basePath())->run();

        // Delete .git
        $this->files->deleteDirectory($this->getPath('.git'));

        // fetch all files
        $files = $this->files->allFiles($this->getPath());

        foreach ($files as $file) {
            $basename = $file->getBasename();
            $finalName = $this->getUpdatedPath($file->getBasename());
            $mergedContent = $this->processReplacement($file->getContents());

            if ($finalName !== $basename) {
                $this->files->put(implode('/', [$file->getPath(), $finalName]), $mergedContent);
                $this->files->delete($file->getRealPath());
            } else {
                $this->files->replace($file->getRealPath(), $mergedContent);
            }
        }

        $this->info('Package created');
    }

    protected function getPath($path = '')
    {
        return $this->laravel->basePath(implode('/', [
            strtolower($this->folder),
            strtolower($this->package),
            $path,
        ]));
    }

    protected function processReplacement($string)
    {
        $replace = $this->replacementStrings();

        return str_replace(array_keys($replace), array_values($replace), $string);
    }

    protected function getUpdatedPath($filename)
    {
        return $this->processReplacement($filename);
    }

    protected function replacementStrings()
    {
        return [
            'rapture/skeletons' => strtolower($this->namespace) . '/' . Str::plural(strtolower($this->package)),
            'Rapture\Skeletons' => $this->namespace . '\\' . Str::plural(ucfirst($this->package)),
            'Rapture\\\\Skeletons' => $this->namespace . '\\\\' . Str::plural(ucfirst($this->package)),
            'Bones' => Str::plural(ucfirst($this->object)),
            'Skeletons' => Str::plural(ucfirst($this->package)),
            'bones' => Str::plural(strtolower($this->object)),
            'skeletons' => Str::plural(strtolower($this->package)),
            'Bone' => Str::singular(ucfirst($this->object)),
            'Skeleton' => Str::singular(ucfirst($this->package)),
            'bone' => Str::singular(strtolower($this->object)),
            'skeleton' => Str::singular(strtolower($this->package)),
            'skull' => Str::kebab(strtolower($this->icon)),
            '0000_00_00_000000' => date('Y_m_d_His'),
        ];
    }

    protected function getStub()
    {
        //
    }
}
